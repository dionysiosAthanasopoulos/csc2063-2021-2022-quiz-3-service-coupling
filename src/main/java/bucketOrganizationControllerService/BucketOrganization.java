package bucketOrganizationControllerService;

import bucketManagementControllerService.BucketManagement;
import bucketManagementControllerService.iBucketManagement;


public class BucketOrganization{

	public void splitBucket( int bucketID ){

		iBucketManagement bucketManagement = new BucketManagement();
		bucketManagement.splitBucket( bucketID );
	}

	public void mergeBuckets(int bucketID1, int bucketID2) {

		iBucketManagement bucketManagement = new BucketManagement();
		bucketManagement.mergeBuckets( bucketID1, bucketID2 );
	}
}