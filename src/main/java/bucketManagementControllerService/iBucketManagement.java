package bucketManagementControllerService;

import java.util.List;

import service.Bucket;


public interface iBucketManagement {

	public void splitBucket( int bucketID );

	public void mergeBuckets( int bucketID1, int bucketID2 );

	public void moveObject( int objectID, int fromBucketID, int toBucketID );

	public void listBuckets();

	public void listObjects();

	public void setUpTestBuckets( List<Bucket> b );
}