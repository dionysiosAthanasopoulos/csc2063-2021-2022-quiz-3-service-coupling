package objectMovementControllerService;

import bucketManagementControllerService.BucketManagement;
import bucketManagementControllerService.iBucketManagement;


public class ObjectMovementController {

	public void moveObject( int objectID, int fromBucketID, int toBucketID ){

		iBucketManagement bucketManagement = new BucketManagement();
		bucketManagement.moveObject( objectID, fromBucketID, toBucketID );
	}

	public void listBuckets() {

		iBucketManagement bucketManagement = new BucketManagement();
		bucketManagement.listBuckets();
	}

	public void listObjects() {

		iBucketManagement bucketManagement = new BucketManagement();
		bucketManagement.listObjects();
	}
}